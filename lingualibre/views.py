from django.db import connections
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from enum import Enum

class HealthStatus(Enum):
    OK = "OK"
    ERROR = "Error"
    RUNNING = "Running"
    DOWN = "Down"
    CONNECTED = "Connected"
    DATABASE_ERROR = "Database Error"

class HealthCheckAPIView(APIView):
    """
    Health Check API to monitor system status.
    Checks:
    - Service status
    - Database connection
    """
    
    def get(self, request):
        health_status = {
            "status": HealthStatus.OK.value,
            "service": HealthStatus.RUNNING.value,
            "database": self.check_database(),
        }

        if HealthStatus.DATABASE_ERROR.value in health_status["database"]:
            health_status["status"] = HealthStatus.ERROR.value
            health_status["service"] = HealthStatus.DOWN.value
            http_status = status.HTTP_500_INTERNAL_SERVER_ERROR
        else:
            http_status = status.HTTP_200_OK

        return Response(health_status, status=http_status)

    def check_database(self):
        """Check database connection."""
        try:
            connections["default"].cursor()
            return HealthStatus.CONNECTED.value
        except Exception as e:
            return f"{HealthStatus.DATABASE_ERROR.value}: {str(e)}"
