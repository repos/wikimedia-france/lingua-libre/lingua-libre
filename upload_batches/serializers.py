from rest_framework import serializers
from django.db.models import Count
from upload_batches.models import UploadBatch, Recording


class RecordingSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        # Apply the batch from the context that's provided by the view to the serializer
        validated_data['batch'] = self.context['batch']
        return Recording.objects.create(**validated_data)

    class Meta:
        model = Recording
        exclude = ['batch'] # all fields except batch id (avoid redundancy)


class UploadBatchSerializer(serializers.ModelSerializer):
    recordings_count = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField()

    class Meta:
        model = UploadBatch
        fields = "__all__"
    
    def get_recordings_count(self, obj):
        return Recording.objects.filter(batch=obj.id).count()

    def get_status(self, obj):
        status = Recording.objects.filter(batch=obj.id).values('state')
        status = status.annotate(count=Count("id"))

        # format the result - is there a better way?
        result = {}
        for query_result in status:
            result[query_result['state']] = query_result['count']

        return result
