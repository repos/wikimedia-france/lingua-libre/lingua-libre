from django.urls import reverse
from django.core.files.uploadedfile import SimpleUploadedFile
from rest_framework import status
from rest_framework.test import APITestCase
from upload_batches.models import UploadBatch, Recording
from upload_batches.serializers import UploadBatchSerializer, RecordingSerializer
from model_bakery import baker
from user_profile.models import User

UPLOAD_BATCH_MODEL_NAME = 'upload_batches.UploadBatch'

class GetAllMyUploadBatches(APITestCase):
    def setUp(self):
        self.locutor = baker.make('locutors.Locutor')
        self.user = User.objects.get(id=self.locutor.linked_user.id)
        UploadBatch.objects.create(
            locutor = self.locutor,
            language_qid = "Q150"
        )

        # Create another upload batch with a random locutor
        # It should never come up in the tests, as the locutor is not the current user's
        baker.make(UPLOAD_BATCH_MODEL_NAME)

    def test_get_all_upload_batches(self):
        # get API response
        self.client.force_authenticate(user=self.user)
        url = reverse('upload_batches_my_all')
        response = self.client.get(url, format='json')
        # get data from db
        upload_batches = UploadBatch.objects.filter(locutor=self.locutor.id)
        serializer = UploadBatchSerializer(upload_batches, many=True)
        
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_not_authenticated(self):
        """
        Not-authenticated upload_batches_my_all should return 403
        """
        # get API response
        url = reverse('upload_batches_my_all')
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class CreateNewUploadBatch(APITestCase):
    # TODO: make sure you can't provide a value for created, started or finished time?
    def setUp(self):
        # create a bogus locutor
        self.locutor = baker.make('locutors.Locutor')
        self.user = User.objects.get(id=self.locutor.linked_user.id)

    def test_create_new_upload_batch(self):
        """
        Creating a new upload batch should store it in the db and send back the data (especially the id).
        """
        # get API response
        self.client.force_authenticate(user=self.user)
        url = reverse('upload_batches_my_all')
        data = {
            'locutor': self.locutor.id,
            'language_qid': 'Q5885'
        }
        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(UploadBatch.objects.count(), 1)
        self.assertEqual(UploadBatch.objects.get().language_qid, 'Q5885')
    
    def test_bad_request_no_locutor_id(self):
        """
        Trying to create without all the required data (locutor, language_qid) should return 400 with explanation of the errors.
        """
        # get API response
        self.client.force_authenticate(user=self.user)
        url = reverse('upload_batches_my_all')
        data = {
            'language_qid': 'Q5885'
        }
        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(UploadBatch.objects.count(), 0)
        self.assertEqual(response.data, {"locutor": ["This field is required."]})
    
    def test_bad_request_invalid_locutor(self):
        """
        Trying to create with a non-existent locutor should return 400 with explanation of the errors.
        """
        # get API response
        self.client.force_authenticate(user=self.user)
        url = reverse('upload_batches_my_all')
        data = {
            'locutor': 15551,
            'language_qid': 'Q5885'
        }
        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(UploadBatch.objects.count(), 0)
        self.assertEqual(response.data, {"locutor": ['Invalid pk "15551" - object does not exist.']})

    def test_not_authenticated(self):
        """
        Not-authenticated upload_batches_my_all should return 403.
        """
        # get API response
        url = reverse('upload_batches_my_all')
        response = self.client.post(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class GetSingleMyUploadBatch(APITestCase):
    def setUp(self):
        self.batch = baker.make(UPLOAD_BATCH_MODEL_NAME)
        self.user = User.objects.get(id=self.batch.locutor.linked_user.id)

        # create another batch for another user's locutor
        self.other_batch = baker.make(UPLOAD_BATCH_MODEL_NAME)
    
    def test_get_single_upload_batch(self):
        """
        Returns 200 OK with the appropriate upload batch
        """
        # get API response
        self.client.force_authenticate(user=self.user)
        url = reverse('upload_batches_my_single', kwargs={'id': self.batch.id})
        response = self.client.get(url, format='json')

        # get data from db
        upload_batch = UploadBatch.objects.get(id=self.batch.id)
        serializer = UploadBatchSerializer(upload_batch)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)
    
    def test_not_found_non_existent(self):
        """
        Should return 404 Not Found if an Upload Batch with this id does not exist
        """
        # get API response
        self.client.force_authenticate(user=self.user)
        url = reverse('upload_batches_my_single', kwargs={'id': 1551})
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
    
    def test_not_found_not_one_of_own(self):
        """
        Should return 404 Not Found if an Upload Batch with this id is not one of the user's locutors'
        """
        # get API response
        self.client.force_authenticate(user=self.user)
        url = reverse('upload_batches_my_single', kwargs={'id': self.other_batch.id})
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
    
    def test_not_authenticated(self):
        """
        Not-authenticated upload_batches_my_single should return 403
        """
        # get API response
        url = reverse('upload_batches_my_single', kwargs={'id': 1})
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class UploadRecording(APITestCase):
    """
    Tests uploading recordings
    """

    def setUp(self):
        self.batch = baker.make(UPLOAD_BATCH_MODEL_NAME)
        self.user = User.objects.get(id=self.batch.locutor.linked_user.id)

    def test_create_new_record(self):
        """
        Creating a new recording should store it in the db and send back the data (especially the id).
        """
        # get API response
        self.client.force_authenticate(user=self.user)
        url = reverse('upload_batches_my_single_recordings', kwargs={'id': self.batch.id})
        audio = SimpleUploadedFile('pomme.wav', b'content', content_type="audio/wav")
        data = {
            "file": audio,
            "transcription": "pomme"
        }
        response = self.client.post(url, data)

        # get data from db
        recordings = Recording.objects.filter(batch=self.batch).get()
        serializer = RecordingSerializer(recordings)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data, serializer.data)

    def test_bad_request_missing_file(self):
        """
        Trying to create without the file should return 400 with explanation of the errors.
        """
        # get API response
        self.client.force_authenticate(user=self.user)
        url = reverse('upload_batches_my_single_recordings', kwargs={'id': self.batch.id})
        data = {
            "transcription": "pomme"
        }
        response = self.client.post(url, data)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Recording.objects.filter(batch=self.batch).count(), 0)
        self.assertEqual(response.data, {"file": ["No file was submitted."]})

    def test_bad_request_missing_field(self):
        """
        Trying to create without all the required data (transcription) should return 400 with explanation of the errors.
        """
        # get API response
        self.client.force_authenticate(user=self.user)
        url = reverse('upload_batches_my_single_recordings', kwargs={'id': self.batch.id})
        audio = SimpleUploadedFile('pomme.wav', b'content', content_type="audio/wav")
        data = {
            "file": audio
        }
        response = self.client.post(url, data)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Recording.objects.filter(batch=self.batch).count(), 0)
        self.assertEqual(response.data, {"transcription": ["This field is required."]})

    def test_not_found_batch_not_owned(self):
        """
        Trying to create with a batch owned by another user's locutor should return 404.
        """
        # Create another batch from scratch (so with another user)
        other_batch = baker.make(UPLOAD_BATCH_MODEL_NAME)

        # get API response
        self.client.force_authenticate(user=self.user)
        url = reverse('upload_batches_my_single_recordings', kwargs={'id': other_batch.id})
        response = self.client.post(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_not_found_invalid_batch(self):
        """
        Trying to create with a non-existent batch should return 404.
        """
        # get API response
        self.client.force_authenticate(user=self.user)
        url = reverse('upload_batches_my_single_recordings', kwargs={'id': 1551})
        response = self.client.post(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_not_authenticated(self):
        """
        Not-authenticated upload_batches_my_single_recordings should return 403
        """
        # get API response
        url = reverse('upload_batches_my_single_recordings', kwargs={'id': self.batch.id})
        response = self.client.post(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
