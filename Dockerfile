
# Base image
FROM python:3.11-slim

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Set work directory
WORKDIR /app

RUN apt-get update && apt-get install -y \
    libpq-dev gcc pkg-config libmariadb-dev-compat libmariadb-dev curl \
    && apt-get clean

COPY wait-for-it.sh /app/wait-for-it.sh
RUN chmod +x /app/wait-for-it.sh

# Install Python dependencies
COPY requirements.txt /app/
RUN pip install -r requirements.txt

# Copy project files to the container
COPY . /app

# Expose the port the app runs on
EXPOSE 8080

# Command to run the Django development server
CMD ["/app/wait-for-it.sh", "mariadb:3306", "--", "sh", "-c", "python manage.py migrate && python manage.py runserver 0.0.0.0:8080"]
