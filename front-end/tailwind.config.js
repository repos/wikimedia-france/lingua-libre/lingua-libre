/** @type {import('tailwindcss').Config} */
export default {
  content: [],
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        'primary-blue': '#3366cc',
        'light-blue': '#206ae2',
      },
      screens: {
        xs: '480px',
      },
    },
  },
  plugins: [],
}
