export default {
    // Static website
    "homeHeading": "Welcome to Lingua Libre, the participative linguistic media library of Wikimedia France.",

    // App : common
    "rw-common-previous":"Previous",
    "rw-common-next":"Next",
    "rw-common-save":"Save",
    "rw-common-cancel":"Cancel",
    // "rw-common-add":"Add",
    // "rw-common-delete":"Delete",
    // "rw-common-add":"Play",
    // "rw-common-stop":"Stop",
    "rw-common-noresults":"No results found.",
    "rw-common-input":"Input your choice.",
    "rw-common-wikipedia":"Wikipedia",
    "rw-common-wiktionary":"Wiktionary",
    // "rw-common-wikisource":"Wikisource", // <- not activate due to heavy cascading templates obfuscating its wikitext

    // App: Steps
    "rw-locutor-step":"Locutor",
    "rw-locutor-title":"Pick your locutor",
    "rw-locutor-menu-languages":"languages", // -> language(s)
    "rw-locutor-menu-add":"Add locuter", // -> "Add locutor"
    "rw-locutor-required":"This locutor is your main locutor. It cannot be removed, and you must use it if recording words on your own.",
    "rw-locutor-name-title":"Name",
    "rw-locutor-name-guideline":"Please enter the locutor's name.",
    "rw-locutor-name-footer":"Note that once this locutor has at least one recording, this name can no longer be changed.",
    "rw-locutor-languages-title":"Spoken or signed languages ",
    "rw-locutor-languages-language-placeholder":"Language",
    "rw-locutor-languages-learning_place-placeholder":"Place of learning",
    "rw-locutor-languages-add":"Add language",
    "rw-locutor-residence-title":"Place of residence",
    "rw-locutor-residence-placeholder":"Enter place name", // <- use "rw-common-input"
    "rw-locutor-licence-title":"License",
    "rw-locutor-licence-guideline":"License under which your recordings will be released on Wikimedia Commons.", // <- string changed
    "rw-locutor-CC0":"CC0",  // <- string changed
    "rw-locutor-CCBY4.0":"CCBY4.0", // <- string changed
    "rw-locutor-CCBYSA4.0":"CCBYSA4.0", // <- string changed
    "rw-locutor-delete-title":"Delete this locutor",
    "rw-locutor-delete-guideline":"You can only delete locutors that do not have any recordings associated to them.",
    "rw-locutor-delete-modal":"Are you absolutely sure?", // <- use "rw-locutor-delete-title"
    "rw-locutor-delete-action":"You are about to delete this locutor.",
    "rw-locutor-delete-warning":"The deletion is irreversible.",
    "rw-locutor-delete-retype":"Enter the following to confirm:",
    "rw-locutor-delete-cancel":"Cancel, keep locutor", // <- use "rw-common-cancel"
    "rw-locutor-delete-delete":"Yes, delete locutor", // <- use "rw-common-delete"

    // move back to Step 4 List, top right corner ? 
    "rw-languages-step":"Language",
    "rw-languages-title":"Pick your Recording language",
    "rw-languages-proficiency":"proficiency", // -> "Proficiency"

    "rw-hardware-step":"Hardware Check",
    "rw-hardware-title":"Check your Hardware",
    "rw-hardware-test":"Test current microphone", // <- string changed
    "rw-hardware-settings":"Reopen microphone settings", // <- string changed
    "rw-hardware-waiting":"Waiting",
    "rw-hardware-recording":"Recording",
    "rw-hardware-listening":"Listening",

    "rw-lists-step":"Word List", // -> "List"
    "rw-lists-title":"Word list", // -> "List"
    "rw-lists-placeholder":"Type here the word too record", // -> "Enter elements to record"
    "rw-lists-hint-homographs":"To differentiate heterophone homographs, specify the distinction with parenthesis : “axes (weapon)” vs “axes (geometry)”.", // <- string changed
    "rw-lists-hint-edit":"To add several elements at once, separate them with #, for example “big#house#orange”.", // <- string changed
    "rw-lists-alert-few":"Only $1 words are available in this list", // <- string changed

    "rw-lists-filter-clear":"Clear",
    "rw-lists-filter-limit":"Number of words to get?",
    "rw-lists-filter-remove-by-community":"Remove elements previously recorded", // <- string changed
    "rw-lists-filter-remove-by-user":"Exclude elements <i>you</i> previously recorded", // <- string changed

    "rw-lists-generators-local-title":"Local lists",
    "rw-lists-generators-local-title":"Title", // -> "List title"

    "rw-lists-generators-nearby-title":"Nearby",
    "rw-lists-generators-nearby-coordinates":"Coordinates",
    "rw-lists-generators-nearby-latitude":"Latitude",
    "rw-lists-generators-nearby-longitude":"Longitude",
    "rw-lists-generators-nearby-rejected":"User denied the request for Geolocation",

    "rw-lists-generators-categories-title":"Wikimedia Categories",    
    "rw-lists-generators-categories-source":"Source",
    "rw-lists-generators-categories-project":"Choose an option", // -> "Select project"
    "rw-lists-generators-categories-code":"Choose an option", // -> "Select language"
    "rw-lists-generators-categories-category":"Category name", // -> "Enter category's name"

    "rw-lists-generators-external-title":"External tools",
    "rw-lists-generators-external-query-title":"ExternalTools URL (PetScan, Wikidata query service)",
    "rw-lists-generators-external-query-placeholder":"Enter URL",

    "rw-lists-share-title":"Share list",
    "rw-lists-share-link-update":"Update link",
    "rw-lists-share-link-new":"New link",
    "rw-lists-share-link-hint":"If you add words inside the list after clicking on share list, you can decide to either add those words within same link, or create a new link.",

    "rw-record-step":"Record",
    "rw-record-title":"Record",
    "rw-record-guideline":"Click the toggle button to start recording.", // -> "Click button to start/stop recording"
    "rw-record-next":"Skip to the next word", // -> use rw-common-next
    "rw-record-start":"Start recording", // => remove text, replace by red circle button ?

    "rw-review-step":"Review",
    "rw-review-title":"Review your recordings",
    "rw-review-guideline":"Check your records before starting their publication.",
    "rw-review-publish":"Publish !",
    "rw-review-continue":"Record More words", // -> "Record more" 
    "rw-review-stored":"Recordings successfully stored !",
    "rw-review-state":"They are currently put on hold and will soon be uploaded to Wikimedia Commons.",
    "rw-review-progress":"Check out the progress here.", // -> Check progress

    // App : Settings
    "rw-settings-title":"Audio settings",
    "rw-settings-type-title":"Recording type", // => `-type` group : move to Step 4 List, top right corner ? 
    "rw-settings-type-words":"Words",
    "rw-settings-type-sentences":"Sentences",
    "rw-settings-type-texts":"Poems",
    "rw-settings-level-start":"Start Threshold", // T -> t
    "rw-settings-level-stop":"Stop Threshold", // T -> t
    "rw-settings-silence-duration":"Stop Duration", // D -> d
    "rw-settings-margin-beggining":"Margin Before",  // B -> b
    "rw-settings-margin-end":"Margin After",  // A -> a
    "rw-settings-level-saturation":"Saturation Threshold",  // T -> t

    // App : Shortcuts
    "rw-shortcuts-title":"Keyboard shortcuts",
    "rw-shortcuts-previous":"Previous",  // -> "Previous" -> use rw-common-previous ?
    "rw-shortcuts-next":"Next Word",  // -> "Next" -> use rw-common-next ?
    "rw-shortcuts-space":"space",
    "rw-shortcuts-toggle":"Toggle Recording",  // -> "Start/Stop recording"
    "rw-shortcuts-p":"p",
    "rw-shortcuts-play":"Play Recording",  // R -> r
    "rw-shortcuts-del":"del",
    "rw-shortcuts-delete":"Delete Recording"  // R -> r
}