import { useRecordWizardStore } from '@/stores/recordwizard'
import makeApiCall from './makeApiCall'

export default async () => {
  const store = useRecordWizardStore()
  const { selectedLanguageId, selectedLocutorId } = store
  const recordings = await makeApiCall(
    'get',
    `http://127.0.0.1:8080/api/locutors/my/${selectedLocutorId}/recordings/${selectedLanguageId}`,
    (e) => {},
    true,
    {},
    {},
  )
  return recordings.map(item => item.transcription)
}
