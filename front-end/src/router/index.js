import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path: '/',
    component: () => import('../views/RecordWizardView.vue'),
    children: [
      {
        path: '',
        name: 'record-wizard.locutor',
        component: () => import('../views/LocutorStep.vue'),
      },
      {
        path: 'language',
        name: 'record-wizard.language',
        component: () => import('../views/LanguageStep.vue'),
      },
      {
        path: 'hardware-check',
        name: 'record-wizard.hardware-check',
        component: () => import('../views/HardwareCheckStep.vue'),
      },
      {
        path: 'word-list',
        name: 'record-wizard.word-list',
        component: () => import('../views/ListStep.vue'),
      },
      {
        path: 'record',
        name: 'record-wizard.record',
        component: () => import('../views/RecordStep.vue'),
      },
      {
        path: 'review',
        name: 'record-wizard.review',
        component: () => import('../views/ReviewStep.vue'),
      },
    ],
  },
  {
    path: '/share/:listId', component: () => import('../views/ShareList.vue')
  },
  { path: '/my-upload-batches', component: () => import('../views/UploadBatchesView.vue') },
  { path: '/my-upload-batches/:id', component: () => import('../views/UploadBatchView.vue') },
]

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
})

export default router
