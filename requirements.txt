# Comments added using ChatGPT
asgiref==3.8.1                # ASGI server reference implementation, used in Django for async support
attrs==23.2.0                 # Handles attributes in Python classes with less boilerplate code
autobahn==23.6.2              # WebSocket and WAMP framework for real-time applications
Automat==22.10.0              # Finite-state machine for better control flow
certifi==2024.2.2             # Provides Mozilla's CA Bundle for HTTPS requests
cffi==1.16.0                  # Foreign Function Interface for calling C code from Python
charset-normalizer==3.3.2     # Detects character encoding in text for proper decoding
constantly==23.10.4           # Symbolic constants library used by Twisted for networking protocols
coverage==7.5.1               # Tool to measure code coverage during testing
cryptography==42.0.7          # Cryptographic functions like SSL, encryption, etc.
daphne==4.1.2                 # ASGI server for running Django channels (WebSocket support)
defusedxml==0.8.0rc2          # Handles XML to prevent security risks
Django==5.0.10                # High-level Python web framework
django-cors-headers==4.3.1    # Django app for handling Cross-Origin Resource Sharing (CORS)
djangorestframework==3.15.1   # Toolkit for building Web APIs in Django
djangorestframework-simplejwt==5.3.1     # JWT-based authentication for Django REST Framework
hyperlink==21.0.0             # URL parsing and processing
idna==3.7                     # Internationalized Domain Names in Applications (IDNA) support
incremental==22.10.0          # Versioning Python projects, used in Twisted
model-bakery==1.18.0          # Factory for Django models during testing
mysqlclient==2.2.4            # MySQL database adapter
oauthlib==3.2.2               # OAuth implementation
pyasn1==0.6.0                 # ASN.1 data structures used in networking (e.g., SSL, certificates)
pyasn1_modules==0.4.0         # Protocol modules (e.g., X.509, PKCS) for pyasn1
pycparser==2.22               # C parser used in cffi for calling C functions
pydantic_core==2.18.2         # Core validation library for data models (used in Pydantic)
PyJWT==2.8.0                  # JSON Web Token implementation in Python
pyOpenSSL==24.1.0             # Python bindings for OpenSSL, used for secure communications
python3-openid==3.2.0         # OpenID support for authentication
requests==2.32.2              # Simplified HTTP
requests-oauthlib==2.0.0      # OAuth support for Requests library
service-identity==24.1.0      # Verifies SSL/TLS certificates for Twisted
setuptools==70.0.0            # Python package manager and build system
six==1.16.0                   # Compatibility library for Python 2 and 3
social-auth-app-django==5.4.1 # Django integration for social authentication (e.g., Google, Facebook)
social-auth-core==4.5.4       # Core library for social authentication
sqlparse==0.5.0               # SQL parsing and formatting
Twisted==24.3.0               # Event-driven networking engine
txaio==23.1.1                 # Compatibility layer for async programming between Twisted and asyncio
typing_extensions==4.12.0     # Backports of new typing features in Python
tzdata==2024.1                # Timezone database for date and time handling
urllib3==2.2.1                # HTTP client, used in requests
zope.interface==6.4.post2     # Interface definitions

# Windows-only package
twisted-iocpsupport==1.0.4; sys_platform == 'win32' # Async I/O support for Twisted on Windows