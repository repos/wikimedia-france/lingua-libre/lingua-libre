# Locutors

A **Locutor** is an **individual** that records audio or video files.
It is created, managed and linked to a single **User** account. A **User** account may have 0 to many **Locutors** linked to it.

## Creating a Locutor

## Deleting a Locutor
A Locutor can be deleted if it has no recordings yet. (?)

## Database model
All models are handled by the `locutors` app.
Model definitions are located in the [`locutors/models.py`](../locutors/models.py) file.

![](diagrams/db_locutors.svg)

### Locutor
Data is stored in the `locutors_locutor` table.
Model definition is as follows:

| Field | Description | Required | Editable at all times | Editable if no recordings yet |
|---|---|---|---|---|
| Name | Display name of this locutor | ✅ | ❌ | ✅ |
| Linked User | User linked to this locutor | ✅ | ❌ | ❌ |
| Gender | Gender of this locutor | ❌ | ? | ? |
| Place of residence | Qid of a location | ❌ | ✅ | ✅ |

### UsesLanguage
| Field | Description | Required |
|---|---|---|
| Proficiency | Mimics the Babel levels: 1-4 and N(ative) | ✅ |
| Place of learning | Qid of a location | ❌ |

#### Proficiency
Defined in [`locutors.models.LanguageProficiency`](../locutors/models.py):
* 1: Beginner
* 2: Average
* 3: Good
* 4: Professional
* 5: Native

## API
links to associated API routes?

## Migrating data from LLv2 Wikibase

This table provides correlations for the existing locutors data on the Lingua Libre 2.x Wikibase and the Lingua Libre 3.x database model.

| LLv2 Wikibase | LLv3 Database |
|---|---|
| Label (`rdfs:label`) | `locutors_locutor.name` |
| Linked user (`prop:P11`) | (how to get it?) -> `locutors_locutor.linked_user` |
| Place of residence (`prop:P14`) | `locutors_locutor.place_of_residence` |
| Sex or gender (`prop:P8`) | `locutors_locutor.gender` |
| Language (`prop:P4`) | ? |
| ↳ Language Level (`prop:P16`) | `locutors_useslanguage.proficiency`|
| ↳ Place of learning (`prop:P15`) | ? |

### Language Level -> Proficiency
_See also WDQS https://w.wiki/BMKo_.

* beginner (`item:Q12`): `LanguageProficiency.BEGINNER` (`1`); wd:Q130379814 (CEFR A level: beginner) 
* average level (`item:Q13`): `LanguageProficiency.AVERAGE` (`2`); wd:Q130379815 (CEFR B level: intermediate)
* good level (`item:Q14`): `LanguageProficiency.GOOD` (`3`); wd:Q104381941 (CEFR C1 level: advanced)
* professional level, not-native (*not modelled on Lingua Libre*): `LanguageProficiency.PROFESSIONAL` (`4`);  wd:Q104381943 (CEFR C2 level: upper advanced)
* native speaker (`item:Q15`): `LanguageProficiency.NATIVE` (`5`); wd:Q130379758 (native speaker level)

## Eventual upcoming features

- per locutor license
- multi-licensing ?
- limit some data visibility (public/restricted/researcher only)?
- put some kind of history for language mastery levels and places of residence?
