# Upload Batches

An **Upload Batch** (UB) is a **collection** of Recordings that are in the *Recordings Processing Pipeline*.
It is linked to a **Locutor**.
A **Locutor** may have 0 to many **Upload Batches** linked to it.
An **Upload Batch** may have 1 to many Recordings linked to it.

## Creating an Upload Batch
It is created once an **User** has reviewed their recordings in the Record Wizard and has uploaded them to Lingua Libre.

## Deleting an Upload Batch
It is automatically deleted 7 days after all its Recordings have been uploaded to Wikimedia Commons (`finished_time != null`), or is kept in the event of an issue with any or all of its Recordings that has yet to be acknowledged by the **User**.

## Determining the state of an Upload Batch
The Upload Batch does not have an explicitly-stated "status" value.
It can be inferred from the presence (or absence thereof) of a value in some of its fields.

* `started_time` and `finished_time` are null: the UB is **pending** its entry in the *Recordings Processing Pipeline*, i.e. no Recordings are currently being uploaded;
* `started_time` is not null: the UB's Recordings are currently **being uploaded** to Wikimedia Commons;
* `finished_time` is not null: the UB's Recordings have **all been uploaded** to Wikimedia Commons.

## Database model
All models are handled by the `upload_batches` app.
Model definitions are located in the [`upload_batches/models.py`](../upload_batches/models.py) file.

![](diagrams/db_upload_batches.svg)
