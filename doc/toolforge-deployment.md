## Deploy on Toolforge

Toolforge is a cloud-based platform that allows developers to host and deploy applications for Wikimedia projects. Deploying a Django application like **Lingua Libre** on Toolforge requires specific configurations. Below is a step-by-step guide on how I successfully deployed it.

## Server Setup

### SSH and Clone Repository

```bash
# SSH into Toolforge
ssh -i ~/.ssh/id_rsa adityasuthar07@login.toolforge.org  
become lingua-libre
# Clone repository into the required directory
git clone https://gitlab.wikimedia.org/repos/wikimedia-france/lingua-libre/lingua-libre.git ./www/python/src
```

### Create `app.py`

On Toolforge, **WSGI** for Django must *also* be declared in `app.py` with the necessary WSGI configuration, It also expects the variable to be named `app` (source: [Toolforge Django OAuth Guide](https://wikitech.wikimedia.org/wiki/Help\:Toolforge/My_first_Django_OAuth_tool#:~\:text=create%20a%202nd-,app.py,-in%20~/www/python)).

```python
# /www/python/src/app.py
import os  
from django.core.wsgi import get_wsgi_application
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'lingualibre.settings')  
app = get_wsgi_application()
```

### Adapt `settings.py`

Modify the `settings.py` file for proper deployment on Toolforge, update static files and hosts related settings:

```python
# /www/python/src/lingualibre/settings.py
STATIC_URL = 'static/'  
STATIC_ROOT = os.path.join(BASE_DIR, "static")  
ALLOWED_HOSTS = ['toolforge.org', 'lingua-libre.toolforge.org']  
CSRF_TRUSTED_ORIGINS = ['https://lingua-libre.toolforge.org']
```

## Python Environment and Webservice Setup

### Set Up Python Virtual Environment

```bash
cd www/python/
# Open a Toolforge webservice shell
webservice --backend=kubernetes python3.11 shell  
# Create and activate a virtual environment
python3 -m venv ~/www/python/venv
source ~/www/python/venv/bin/activate
# Upgrade pip
pip install --upgrade pip
```

### Install Dependencies and Apply Migrations

```bash
cd ./src
# Install required dependencies
pip install -r requirements.txt
# Run Django Migrations & Collect Static Files
python manage.py migrate  
python manage.py collectstatic --noinput  # This will create the static/ directory
```

## Configure `*.ini` Files

Create `python/src/config.ini`, see [Lingua Libre GitLab Setup](https://gitlab.wikimedia.org/repos/wikimedia-france/lingua-libre/lingua-libre#manually).

uWSGI is a high-performance application server that runs Python web applications. It helps serve static files efficiently in a production environment and ensures Django applications function smoothly on Toolforge.

Create a `python/uwsgi.ini` defining the path to the `/static` folder.

```ini
[uwsgi]
check-static = /data/project/lingua-libre/www/python/src
```

## Start the Webservice

```bash
# Create Superuser (recommended)
python manage.py createsuperuser
# Restart the webservice (or use start/stop/status as needed)
webservice --backend=kubernetes python3.11 restart
```

## Front
### Toolforge SSH keys
```bash
# Connect to Toolforge
ssh <username>@login.toolforge.org                      # Note: Replace <username> with your username
ssh-keygen -t ed25519 -C "lingualibre" -f ~/.ssh/id_rsa_gitlab -N ""   # Generates a new SSH key pair
cat ~/.ssh/id_rsa_gitlab.pub > ~/.ssh/authorized_keys   # Move public key to proper Toolforge place
cat ~/.ssh/id_rsa_gitlab.pub                            # Copy public key to your Tooladmin's ssh keys, see below
base64 -w0 ~/.ssh/id_rsa_gitlab                         # Inlined-base64 private key to copy to gitlab CI variables, see below
# chmod 600 ~/.ssh/*                                    # Secure ssh keys
exit
```

Go to your [tooladmin settigs ssh](https://toolsadmin.wikimedia.org/profile/settings/ssh-keys/), add public key from `~/.ssh/id_rsa_gitlab.pub`.

### Gitlab settings CI/CD variables
Open Gitlab > Settings > CI/CD > Variables > Click "Add new variable". Add the following variables:

| Key | Value | Additional description
|---|---|---|
| `SSH_PRIVATE_KEY` | Paste output of previous `base64 -w0 ~/.ssh/id_rsa_gitlab` | Allows deployment to Toolforge
| `TOOLFORGE_USERNAME` | `<your toolforge username>` | 
| `TOOLFORGE_HOST` | `login.toolforge.org` | 

### In gitlab-ci.yml
Create a `.gitlab-ci.yml` with build and deployment commands. Gitlab CI/CD pipeline will use it to build and deploy the front to Toolforge.


## Directory Structure

```
python/  
├── src  
│   ├── app.py  
│   ├── config.ini  
│   ├── manage.py  
│   ├── static  
│   ├── lingualibre  
│   │   ├── settings.py
│   ├── requirements.txt  
│   ├── .gitlab-ci.yml
│   └── ... (other project files)  
├── uwsgi.ini  
└── venv  
	├── bin  
	├── lib  
	├── pyvenv.cfg
```

## References

- [Toolforge Documentation](https://wikitech.wikimedia.org/wiki/Help\:Toolforge)
- [Deploying Django on Toolforge](https://wikitech.wikimedia.org/wiki/Help\:Toolforge/My_first_Django_OAuth_tool#Deploying_to_Wikimedia_Toolforge)
- [Gitlab settings CI/CD](https://docs.gitlab.com/ci/)