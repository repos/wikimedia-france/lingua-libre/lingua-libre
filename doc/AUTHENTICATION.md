# Authentication

Authentication to Lingua Libre is provided by an OAuth consumer handled by a wiki of the Wikimedia Foundation.
Therefore, users may not directly sign up to lingualibre.org, but rather log in to it through their Wikimedia account.
As per the OAuth specifications, no actual credentials are shared to lingualibre.org during this process: the WMF wiki only acknowledges that the logging-in user agreed to the permissions (later referred as "scopes") required by lingualibre.org and provides us with its nickname, CentralAuth user ID (which is a unique numerical ID across all WMF wikis), and user-revokable tokens that allows lingualibre.org to act on the user's behalf (in the limits imposed by the previously agreed-to scopes).

## Technical stuff
OAuth authentication is done through the Lingua Libre backend.
It is handled by the `Social Auth` Django App, which provides a MediaWiki-capable OAuth backend.
OAuth authentication is therefore possible with any MediaWiki-powered wiki that uses [Extension:OAuth](https://www.mediawiki.org/wiki/Extension:OAuth), as is the case of all WMF wikis.

**/!\ `Social Auth` does not support OAuth 2.0. The consumer's protocol version must be set to OAuth 1.0a.**

Most of the instructions here derive from Wikitech's [Help:Toolforge/My first Django OAuth tool](https://wikitech.wikimedia.org/wiki/Help:Toolforge/My_first_Django_OAuth_tool) tutorial, which, while slightly outdated, is still relevant to the task of setting up a development environment for OAuth authentication with Django.

When the user tries to login to Lingua Libre, the Vue app redirects them to the Django's OAuth endpoint (`https://<url>/oauth/login/mediawiki`).
The OAuth handoff occurs, and once completed, the user gets redirected to (`https://<url>/oauth/completed/mediawiki`) which then redirects them to the Vue app's homepage (the exact address being defined in `LOGIN_REDIRECT_URL` in `settings.py`).
This convoluted handling allows the Vue app to receive Django's `sessionid` and `csrftoken` cookies, as they both run on the same domain.

The `sessionid` cookie is then used in any API query that requires authentication.

### Wikimedia account details
When a user logs into lingualibre.org through OAuth, the following data is gathered and stored in the following tables/columns:
* Username (`user_profile_user.username`)
* CentralAuth ID (`social_auth_usersocialauth.uid`), sometimes referred to as `sub` in MediaWiki's OAuth-related documentation

## OAuth in a development environment
In the default development environment, we use MariaDB as the database backend.

To make sure the API queries work (due to CSRF/CORS restrictions), DO NOT QUERY localhost, use 127.0.0.1:port !!

### Register the consumer
localhost

https://meta.wikimedia.beta.wmflabs.org/wiki/Special:OAuthListConsumers/view/77c7a7b4de7c12d489f4587159a35d8f

Uploads to the Beta Cluster are failing, thus I created a localhost-only OAuth 1.0a consumer on the actual Meta.
If this is for the localhost-only, do not forget to:
- set the callback URL to: http://127.0.0.1:8080/ (http, not https, and 127.0.0.1, not localhost!!!)
- allow consumer to specify a callback in requests and use "callback" URL above as a required prefix

https://meta.wikimedia.org/wiki/Special:OAuthListConsumers/view/addb0f7325296780a5c6cdee089b9619

The following procedure applies to the Beta Cluster and the development deployment of Lingua Libre (dev.lingualibre.org).

1. Go to [Special:OAuthConsumerRegistration](https://meta.wikimedia.beta.wmflabs.org/wiki/Special:OAuthConsumerRegistration)
2. Click on `Propose new OAuth 1.0a consumer`.
3. Set the application name, consumer version and description to whatever you want.
4. Set `https://dev.lingualibre.org` (mind the httpS) as the OAuth "callback" URL
5. Check the "Allow consumer to specify a callback in requests and use "callback" URL above as a required prefix." checkbox
6. Set applicable project to `commonswiki`
7. Choose "Request authorization for specific permissions"
8. Select the following scopes: "Create, edit, and move pages"; "Upload new files"; "Upload, replace, and move files"
9. Keep other options as is.
10. Acknowledge.
11. Click on "Propose consumer"

The consumer key and secret will be generated.
While the key is public, the secret **cannot be recovered after leaving the page**.
If you lost the secret, you can ask for a new one to be generated in the `Special:OAuthConsumerRegistration/update/<CONSUMER_KEY>` page.

### Create a Super User
"command" = "command that gets run with `manage.py` (so you might need to do `.venv/bin/python manage.py THE_COMMAND`)"

You can create a super-user with the `createsuperuser` command.

**NOTE:** This user won't have any OAuth credentials attached to it.
This will make most of Lingua Libre's Wikimedia-related features inoperative for this user.
It should therefore only be used for testing purposes, such as manually populating the database with data through the Admin interface.

However, you can turn any existing user (even those created through OAuth) into a super-user manually.
If you already have a super-user with access to the Admin interface, you can easily turn any user you wish into a staff (user gets access to the Admin interface) or super-user (user gets all permissions).
If you don't, you need an SSH access to the server in order to manually edit the database.
Run the `dbshell` command to get into the SQLite/MariaDB shell, and manually set the user to be a super-user and/or staff:
```
> UPDATE user_profile_user SET is_staff=1,is_superuser=1 WHERE id=<USER_ID>;
```
