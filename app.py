"""
Toolforge uses wsgi for django deployment and it will look for an app.py file parallel to manage.py
it should contain the wsgi related configuration with an variable app.
Although it is present in lingualibre/wsgi.py but we have to keep it here also
Refer: https://wikitech.wikimedia.org/wiki/Help:Toolforge/My_first_Django_OAuth_tool#:~:text=create%20a%202nd-,app.py,-in%20~/www/python
"""


import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'lingualibre.settings')

app = get_wsgi_application()
