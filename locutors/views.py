from locutors.models import Locutor, UsesLanguage, ShareableLists
from locutors.serializers import LocutorSerializer, UsesLanguageSerializer, RecordingSerializer, WordListSerializer
from rest_framework import status
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.exceptions import NotFound, ValidationError
from django.core.exceptions import BadRequest

from upload_batches.models import Recording


class LocutorList(APIView):
    """
    List all locutors
    """

    def get(self, request, format=None):
        locutors = Locutor.objects.all()
        serializer = LocutorSerializer(locutors, many=True)
        return Response(serializer.data)


class MyLocutorList(APIView):
    """
    List all locutors of the logged-in user
    """
    authentication_classes = [SessionAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        locutors = Locutor.objects.filter(linked_user=request.user.id)
        serializer = LocutorSerializer(locutors, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        # TODO: Prevent settings is_main_locutor to true?
        # TODO: Return 409 Conflict if locutor with the same name exists?

        serializer = LocutorSerializer(
            data={**request.data, 'linked_user': request.user.id})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST),


class LocutorDetail(APIView):
    """
    Retrieve a locutor instance
    """

    def get_locutor(self, id):
        try:
            return Locutor.objects.get(id=id)
        except Locutor.DoesNotExist:
            raise NotFound()

    def get(self, request, id, format=None):
        locutor = self.get_locutor(id)
        serializer = LocutorSerializer(locutor)
        return Response(serializer.data)


class MyLocutorDetail(APIView):
    """
    Retrieve a locutor instance of the logged-in user
    """
    authentication_classes = [SessionAuthentication]
    permission_classes = [IsAuthenticated]

    def get_locutor(self, request, id):
        try:
            return Locutor.objects.filter(linked_user=request.user.id).get(id=id)
        except Locutor.DoesNotExist:
            raise NotFound()

    def get(self, request, id, format=None):
        locutor = self.get_locutor(request, id)
        serializer = LocutorSerializer(locutor)
        return Response(serializer.data)

    def delete(self, request, id, format=None):
        locutor = self.get_locutor(request, id)
        try:
            locutor.delete()
        except BadRequest:
            raise ValidationError({"detail": "Cannot delete this Locutor."})
        return Response({"detail": "Locutor deleted successfully."}, status=status.HTTP_204_NO_CONTENT)

    def put(self, request, id, format=None):
        locutor = self.get_locutor(request, id)
        serializer = LocutorSerializer(
            locutor, data={**request.data, 'linked_user': request.user.id}, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST),


class MyLocutorLanguages(APIView):
    """
    Languages of a logged-in user's locutor.
    """
    authentication_classes = [SessionAuthentication]
    permission_classes = [IsAuthenticated]

    def get_locutor(self, request, id):
        try:
            return Locutor.objects.filter(linked_user=request.user.id).get(id=id)
        except Locutor.DoesNotExist:
            raise NotFound()

    def post(self, request, id, format=None):
        locutor = self.get_locutor(request, id)
        context = {'locutor': locutor}

        serializer = UsesLanguageSerializer(data=request.data, context=context)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class MyLocutorLanguagesDetail(APIView):
    """
    Retrieve a locutor language instance of the logged-in user
    """
    authentication_classes = [SessionAuthentication]
    permission_classes = [IsAuthenticated]

    def get_locutor(self, request, id):
        try:
            return Locutor.objects.filter(linked_user=request.user.id).get(id=id)
        except Locutor.DoesNotExist:
            raise NotFound()

    def delete(self, request, id, language_id, format=None):
        locutor = self.get_locutor(request, id)
        try:
            language = locutor.languages_used.get(id=language_id)
            language.delete()
        except UsesLanguage.DoesNotExist:
            raise NotFound()
        return Response({"detail": "Language deleted successfully."}, status=status.HTTP_204_NO_CONTENT)


class MyLocutorWordLists(APIView):
    """
    Create a shareable link for locutor's list
    """

    # authentication_classes = [SessionAuthentication]
    # permission_classes = [IsAuthenticated]

    def get_list(self, list_id):
        try:
            return ShareableLists.objects.get(id=list_id)
        except ShareableLists.DoesNotExist():
            raise NotFound()

    def get(self, request, id, list_id):
        list = self.get_list(list_id)
        serializer = WordListSerializer(list)
        return Response(serializer.data)

    def post(self, request, id):
        serializer = WordListSerializer(data={**request.data, 'locutor': id})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, id, list_id):
        list = self.get_list(list_id)
        serializer = WordListSerializer(
            list, data={**request.data, 'locutor': id}
        )
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class MyLocutorRecording(APIView):
    """
    Recordings of a logged-in user's locutor.
    """

    authentication_classes = [SessionAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, id, language=None, format=None):
        recordings = Recording.objects.filter(
            batch__locutor_id=id)
        if language:
            recordings = recordings.filter(batch__language_qid=language)
        serializer = RecordingSerializer(recordings, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
