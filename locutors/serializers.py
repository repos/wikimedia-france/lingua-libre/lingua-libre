from rest_framework import serializers
from locutors.models import Locutor, UsesLanguage, ShareableLists
from upload_batches.models import Recording, UploadBatch


class UsesLanguageSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        # Apply the locutor from the context that's provided by the view to the serializer
        validated_data['locutor'] = self.context['locutor']
        return UsesLanguage.objects.create(**validated_data)

    class Meta:
        model = UsesLanguage
        # all fields except id and locutor (avoid redundancy)
        exclude = ['id', 'locutor']


class LocutorSerializer(serializers.ModelSerializer):
    languages_used = UsesLanguageSerializer(many=True)
    can_be_deleted = serializers.SerializerMethodField()

    class Meta:
        model = Locutor
        fields = '__all__'
        read_only_fields = ['is_main_locutor']

    def create(self, validated_data):
        # Extract the nested languages data
        languages_data = validated_data.pop('languages_used')

        # Create the Locutor instance without the languages data
        locutor = Locutor.objects.create(**validated_data)

        # Create related UsesLanguage instances using the serializer
        for language_data in languages_data:
            language_serializer = UsesLanguageSerializer(
                data=language_data, context={'locutor': locutor})
            if language_serializer.is_valid(raise_exception=True):
                language_serializer.save(locutor=locutor)

        return locutor

    def update(self, instance, validated_data):
        languages_data = validated_data.pop('languages_used', None)

        # Update the Locutor instance
        for attr, value in validated_data.items():
            setattr(instance, attr, value)
        instance.save()

        if languages_data is not None:
            # Remove all existing UsesLanguage entries for this locutor
            instance.languages_used.all().delete()

            # Create related UsesLanguage instances using the serializer
            for language_data in languages_data:
                language_serializer = UsesLanguageSerializer(
                    data=language_data, context={'locutor': instance})
                if language_serializer.is_valid(raise_exception=True):
                    language_serializer.save(locutor=instance)

        return instance

    def get_can_be_deleted(self, obj):
        return obj.can_be_deleted


class RecordingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Recording
        fields = '__all__'


class WordListSerializer(serializers.ModelSerializer):

    class Meta:
        model = ShareableLists
        fields = '__all__'

    def update(self, instance, validated_data):
        # Specify which fields can be updated
        # Example: Only allow updating the 'name' field
        allowed_updates = ['words', 'language_qid']

        for field in allowed_updates:
            if field in validated_data:
                setattr(instance, field, validated_data[field])

        instance.save()
        return instance
